﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts {
    public class TargetLine : MonoBehaviour {//МОЖНО ЛУЧШЕ класс написан на скорую руку и абсолютно не оптимизирован
        public RectTransform rectTransform;
        public RectTransform rectTransformLine;
        [ReadOnly] public UnitOnField startUnitOnField;
        [ReadOnly] public UnitOnField endUnitOnField;
        public Image lineImage;

        public void Setup(UnitOnField newStartUnitOnField, UnitOnField newEndUnitOnField) {
            startUnitOnField = newStartUnitOnField;
            endUnitOnField = newEndUnitOnField;
            lineImage.color = GP.GetColorByTeamId(newStartUnitOnField.teamId);
        }

        public void UpdateView() {
            if (startUnitOnField != null && endUnitOnField != null) {
                rectTransform.position = startUnitOnField.rectTransform.position;
                rectTransform.LookAt(endUnitOnField.rectTransform.position + Vector3.left * 0.01f);
                var dis = (startUnitOnField.rectTransform.anchoredPosition - endUnitOnField.rectTransform.anchoredPosition).magnitude;
                rectTransformLine.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, GP.GetWidthByTeamId(startUnitOnField.teamId));
                rectTransformLine.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, dis);
                lineImage.gameObject.SetActive(true);
            } else {
                lineImage.gameObject.SetActive(false);
            }
        }
    }
}