﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts {
    public enum StateUnit {
        none,
        idle,
        tryFindEnemy,
        move,
        attack,
        die
    }

    public class UnitOnField : MonoBehaviour {
        [ReadOnly] public int teamId;
        [ReadOnly] public int index;
        public StateUnit currentStateUnit = StateUnit.none;
        [ReadOnly] public CellOnField currentCellOnField;
        public RectTransform rectTransform;
        public Image hpBarImage;
        public Image bodyImage;
        private Coroutine _moveCoroutine;
        public StrategyFindPath findPath;
        public TargetLine currentTargetLine;
        private bool _canAttack;

        private int _currentHP;
        public int currentHP {
            get { return _currentHP; }
            set {
                _currentHP = value;
                UpdateViewHpBar();
            }
        }

        private UnitOnField _targetUnitOnField;
        public UnitOnField targetUnitOnField {
            get { return _targetUnitOnField; }
            set {
                _targetUnitOnField = value;
                currentTargetLine.Setup(this, value);
            }
        }

        public void Start() {
            findPath = new FindPathTypeA();
        }

        public void Update() {
            switch (currentStateUnit) {
                case StateUnit.none:
                    break;
                case StateUnit.idle:
                    SetStateUnit(StateUnit.tryFindEnemy);
                    break;
                case StateUnit.tryFindEnemy:
                    DoOnTryFindEnemy();
                    break;
                case StateUnit.move:
                    DoOnMove();
                    break;
                case StateUnit.attack:
                    DoOnAttack();
                    break;
                case StateUnit.die:
                    UnitsController.inst.UnitOnFieldDie(this);
                    break;
                default:
                    SetStateUnit(StateUnit.none);
                    break;
            }
        }

        public void LateUpdate() {
            currentTargetLine.UpdateView();//МОЖНО ЛУЧШЕ подобные обновление вьюхи нужно делать через подписку
        }

        public void Setup(CellOnField newCellOnField, int newTeamId, int newIndex) {
            index = newIndex;
            currentHP = GP.maxUnitHP;
            currentCellOnField = newCellOnField;
            teamId = newTeamId;
            rectTransform.position = currentCellOnField.GetPosition();
            bodyImage.color = GP.GetColorByTeamId(teamId);
            name = teamId + "_" + index + "_unit";
            _canAttack = true;
            SetStateUnit(StateUnit.none);
        }

        public void UpdateViewHpBar() {
            hpBarImage.fillAmount = (float) currentHP / GP.maxUnitHP;
        }

        public void StopMoveCoro() {
            if (_moveCoroutine != null) {
                StopCoroutine(_moveCoroutine);
            }

            _moveCoroutine = null;
        }

        public List<CellOnField> GetEmptyCellOnFieldRoundUnit() {
            var listEmptyCells = new List<CellOnField>();
            var startY = currentCellOnField.y - 1 < 0 ? 0 : currentCellOnField.y - 1;
            var endY = currentCellOnField.y + 1 >= GP.fieldHeight ? GP.fieldHeight - 1 : currentCellOnField.y + 1;
            var startX = currentCellOnField.x - 1 < 0 ? 0 : currentCellOnField.x - 1;
            var endX = currentCellOnField.x + 1 >= GP.fieldWidth ? GP.fieldWidth - 1 : currentCellOnField.x + 1;
            for (var y = startY; y <= endY; y++) {
                for (var x = startX; x <= endX; x++) {
                    var cellOnField = FieldController.inst.GetCellOnFieldByXandY(x, y);
                    if (!cellOnField.isEmpty) continue;
                    if (cellOnField.x == currentCellOnField.x && cellOnField.y == currentCellOnField.y) continue;
                    listEmptyCells.Add(cellOnField);
                }
            }

            return listEmptyCells;
        }


        public void SetStateUnit(StateUnit newStateUnit) {
            currentStateUnit = newStateUnit;
        }

        public void DoOnTryFindEnemy() {
            if (targetUnitOnField == null) {
                var allEnemy = UnitsController.inst.GetAllEnemyForId(teamId);

                foreach (var unitOnField in allEnemy) {
                    if (IsEnemyNear(unitOnField)) {
                        targetUnitOnField = unitOnField;
                        SetStateUnit(StateUnit.move);
                        break;
                    }
                }
                //МОЖНО ЛУЧШЕ поиск первого врага и кратчайшего пути к нему: беруться все враги, вокруг каждого беруться свободные клетки и находится кратчайший путь.
                var allPath = new List<InfoPath>();
                foreach (var enemy in allEnemy) {
                    var allPathToEnemy = GetAllPathsToUnitOnField(enemy);
                    foreach (var infoPath in allPathToEnemy) {
                        allPath.Add(infoPath);
                    }
                }

                var shortInfoPath = GetShortInfoPath(allPath);
                if (shortInfoPath != null) {
                    targetUnitOnField = shortInfoPath.unitOnField;
                    SetStateUnit(StateUnit.move);
                }
            } else {
                SetStateUnit(StateUnit.move);
            }
        }

        public void DoOnMove() {
            if (_moveCoroutine != null) {
                return;
            }

            if (targetUnitOnField == null) {
                SetStateUnit(StateUnit.tryFindEnemy);
                return;
            }


            if (IsEnemyNear(targetUnitOnField)) {
                SetStateUnit(StateUnit.attack);
            } else {
                var paths = GetAllPathsToUnitOnField(targetUnitOnField);
                if (paths != null && paths.Count != 0) {
                    List<OneCell> path = null;
                    path = GetShortInfoPath(paths).path;

                    if (path != null) {
                        var needCell = FieldController.inst.GetCellOnFieldByXandY(path[path.Count - 1].x, path[path.Count - 1].y);
                        StartCoroutine(IEnumMoveToCellOnField(needCell));
                    }
                }
            }
        }

        public void DoOnAttack() {
            if (targetUnitOnField == null || targetUnitOnField.currentStateUnit == StateUnit.die) {
                targetUnitOnField = null;
                SetStateUnit(StateUnit.tryFindEnemy);
                return;
            }

            if (IsEnemyNear(targetUnitOnField)) {
                if (_canAttack) {
                    _canAttack = false;
                    StartCoroutine(IEnumWaitNextAttack());
                    targetUnitOnField.TakeDamage(Random.Range(GP.minDamageUnit, GP.maxDamageUnit));
                }
            } else {
                SetStateUnit(StateUnit.move);
            }
        }
        //МОЖНО ЛУЧШЕ использовать IEnumerator как выдержку времени для атаки не корректно. Так как он зависит от времени Update. При "лагах" устройства время между выстрелами будет увеличено.
        public IEnumerator IEnumWaitNextAttack() {
            yield return new WaitForSeconds(GP.speedAttackUnit);
            _canAttack = true;
        }

        public IEnumerator IEnumMoveToCellOnField(CellOnField cellOnField) {
            var v3 = cellOnField.GetPosition();
            StopMoveCoro();
            currentCellOnField.isEmpty = true;
            cellOnField.isEmpty = false;
            currentCellOnField = cellOnField;
            yield return _moveCoroutine = MainAnimator.inst.StartCoroutine(MainAnimator.inst.IEnumMoveTransform(rectTransform, v3, GP.speedMoveUnit, 1));

            StopMoveCoro();
        }

        public List<InfoPath> GetAllPathsToUnitOnField(UnitOnField enemy) {
            var allPath = new List<InfoPath>();
            var emptyCellRoundEnemy = enemy.GetEmptyCellOnFieldRoundUnit();
            foreach (var cellOnField in emptyCellRoundEnemy) {
                var path = findPath.GetCalculatedPath(FieldController.inst.GetListCellOnFields(), currentCellOnField, cellOnField);
                if (path != null) {
                    var infoPath = new InfoPath {pathLength = path.Count, unitOnField = enemy, path = path};
                    allPath.Add(infoPath);
                }
            }

            return allPath;
        }

        public InfoPath GetShortInfoPath(List<InfoPath> allPath) {
            InfoPath shortInfoPath = null;
            var shortPath = int.MaxValue;
            for (var i = 0; i < allPath.Count; i++) {
                if (allPath[i].pathLength < shortPath) {
                    shortPath = allPath[i].pathLength;
                    shortInfoPath = allPath[i];
                }
            }

            return shortInfoPath;
        }

        public bool IsEnemyNear(UnitOnField unitOnField) {
            var startY = currentCellOnField.y - 1 < 0 ? 0 : currentCellOnField.y - 1;
            var endY = currentCellOnField.y + 1 >= GP.fieldHeight ? GP.fieldHeight - 1 : currentCellOnField.y + 1;
            var startX = currentCellOnField.x - 1 < 0 ? 0 : currentCellOnField.x - 1;
            var endX = currentCellOnField.x + 1 >= GP.fieldWidth ? GP.fieldWidth - 1 : currentCellOnField.x + 1;
            for (var y = startY; y <= endY; y++) {
                for (var x = startX; x <= endX; x++) {
                    var cellOnField = FieldController.inst.GetCellOnFieldByXandY(x, y);
                    if (cellOnField.x == unitOnField.currentCellOnField.x && cellOnField.y == unitOnField.currentCellOnField.y) {
                        return true;
                    }
                }
            }

            return false;
        }

        public void TakeDamage(int damage) {
            var newHp = currentHP - damage;
            currentHP = newHp;
            if (newHp < 0) {
                SetStateUnit(StateUnit.die);
            }
        }
    }

    public class InfoPath {
        public UnitOnField unitOnField;
        public List<OneCell> path;
        public int pathLength;
    }
}