﻿using UnityEngine;

namespace Assets.Scripts {
    public class BattleController : MonoBehaviour {
        public static BattleController inst;

        public void Awake() {
            inst = this;
        }

        public void GenerationTeams() {
            FieldController.inst.SetEmptyAllCellOnField();
            UnitsController.inst.ClearAllUnit();
            UnitsController.inst.CreateTeam(GP.leftTeamId, Random.Range(GP.minCountUnitForTeam, GP.maxCountUnitForTeam + 1), 0, GP.maxColumnForTeam);
            UnitsController.inst.CreateTeam(GP.rightTeamId, Random.Range(GP.minCountUnitForTeam, GP.maxCountUnitForTeam + 1), GP.fieldWidth - GP.maxColumnForTeam, GP.fieldWidth);
            UnitsController.inst.SetStateToAllUnits(StateUnit.idle);
        }
    }
}