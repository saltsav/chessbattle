﻿using System;
using System.Collections.Generic;

namespace Assets.Scripts {
    public interface StrategyFindPath {
        List<OneCell> GetCalculatedPath(List<CellOnField> listCellOnFields, CellOnField startCell, CellOnField finalCell);
    }

    public class FindPathTypeA : StrategyFindPath {//МОЖНО ЛУЧШЕ это простая реализация волнового алгоритма поиска пути. 100% есть куда более оптимальные алгоритмы.
        public OneCell[,] arrayOneCells;
        public List<OneCell> listFirstWave = new List<OneCell>();
        public List<OneCell> listSecondWave = new List<OneCell>();

        public List<OneCell> GetCalculatedPath(List<CellOnField> listCellOnFields, CellOnField startCell, CellOnField finalCell) {
            if (arrayOneCells == null) {
                GenerationArrayField(listCellOnFields);
            } else {
                GenerationArrayField(listCellOnFields, true);
            }

            ResetArrayOneCells();

            var finalCellIsVisited = false;
            var massPath = 0;
            listFirstWave.Clear();
            listSecondWave.Clear();
            listSecondWave.Add(arrayOneCells[startCell.x, startCell.y]);
            AddRoundEmptyCells(arrayOneCells[startCell.x, startCell.y]);
            var flagNumberWave = 1;
            do {
                if (flagNumberWave == 1) {
                    listFirstWave.Clear();
                } else {
                    listSecondWave.Clear();
                }

                if (listFirstWave.Count == 0 && listSecondWave.Count == 0) {
                    break;
                }

                foreach (var cellRound in flagNumberWave == 1 ? listSecondWave : listFirstWave) {
                    if (!cellRound.isVisited) {
                        cellRound.isVisited = true;
                        cellRound.numberMassPath = massPath;
                        if (cellRound.x == finalCell.x && cellRound.y == finalCell.y) {
                            finalCellIsVisited = true;
                        }

                        foreach (var cellRoad in cellRound.roundEmptyCells) {
                            cellRoad.beforeOneCell = cellRound;
                            if (flagNumberWave == 1) {
                                listFirstWave.Add(cellRoad);
                            } else {
                                listSecondWave.Add(cellRoad);
                            }
                        }
                    }
                }

                foreach (var oneCell in flagNumberWave == 1 ? listFirstWave : listSecondWave) {
                    AddRoundEmptyCells(oneCell);
                }

                massPath++;
                if (flagNumberWave == 1) {
                    flagNumberWave = 2;
                } else {
                    flagNumberWave = 1;
                }
            } while (!finalCellIsVisited);

            if (arrayOneCells[finalCell.x, finalCell.y].beforeOneCell != null) {
                var reversPath = new List<OneCell>();
                var cell = arrayOneCells[finalCell.x, finalCell.y];
                do {
                    reversPath.Add(cell);
                    cell = cell.beforeOneCell;
                    if (cell == null || cell.numberMassPath == 0) {
                        break;
                    }
                } while (true);

                return reversPath;
            }

            return null;
        }

        public void ResetArrayOneCells() {
            for (var y = 0; y < GP.fieldHeight; y++) {
                for (var x = 0; x < GP.fieldWidth; x++) {
                    arrayOneCells[x, y].isVisited = false;
                    arrayOneCells[x, y].beforeOneCell = null;
                    arrayOneCells[x, y].numberMassPath = 0;
                }
            }
        }

        public void AddRoundEmptyCells(OneCell oneCell) {
            oneCell.roundEmptyCells.Clear();
            if (oneCell.x + 1 < GP.fieldWidth && !arrayOneCells[oneCell.x + 1, oneCell.y].isVisited && arrayOneCells[oneCell.x + 1, oneCell.y].isEmpty) {
                oneCell.roundEmptyCells.Add(arrayOneCells[oneCell.x + 1, oneCell.y]);
            }

            if (oneCell.x - 1 >= 0 && !arrayOneCells[oneCell.x - 1, oneCell.y].isVisited && arrayOneCells[oneCell.x - 1, oneCell.y].isEmpty) {
                oneCell.roundEmptyCells.Add(arrayOneCells[oneCell.x - 1, oneCell.y]);
            }

            if (oneCell.y + 1 < GP.fieldHeight && !arrayOneCells[oneCell.x, oneCell.y + 1].isVisited && arrayOneCells[oneCell.x, oneCell.y + 1].isEmpty) {
                oneCell.roundEmptyCells.Add(arrayOneCells[oneCell.x, oneCell.y + 1]);
            }

            if (oneCell.y - 1 >= 0 && !arrayOneCells[oneCell.x, oneCell.y - 1].isVisited && arrayOneCells[oneCell.x, oneCell.y - 1].isEmpty) {
                oneCell.roundEmptyCells.Add(arrayOneCells[oneCell.x, oneCell.y - 1]);
            }
        }

        public void GenerationArrayField(List<CellOnField> listCellOnFields, bool isUpdate = false) {
            if (!isUpdate) {
                arrayOneCells = new OneCell[GP.fieldWidth, GP.fieldHeight];
            }

            for (var y = 0; y < GP.fieldHeight; y++) {
                for (var x = 0; x < GP.fieldWidth; x++) {
                    if (!isUpdate) {
                        arrayOneCells[x, y] = new OneCell {
                            x = x,
                            y = y,
                            isEmpty = FieldController.inst.GetCellOnFieldByXandY(x, y).isEmpty
                        };
                    } else {
                        arrayOneCells[x, y].x = x;
                        arrayOneCells[x, y].y = y;
                        arrayOneCells[x, y].isEmpty = FieldController.inst.GetCellOnFieldByXandY(x, y).isEmpty;
                    }
                }
            }
        }
    }

    [Serializable]
    public class OneCell {
        public int x;
        public int y;
        public bool isEmpty;
        public bool isVisited;
        public List<OneCell> roundEmptyCells = new List<OneCell>();
        public OneCell beforeOneCell;
        public int numberMassPath;
    }
}