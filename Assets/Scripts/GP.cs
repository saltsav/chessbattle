﻿using UnityEngine;

namespace Assets.Scripts {
    public static class GP {
        public const int fieldWidth = 8;
        public const int fieldHeight = 6;

        public static Color colorGray = new Color(0.9f, 0.9f, 0.9f, 1);

        public const int minCountUnitForTeam = 2;
        public const int maxCountUnitForTeam = 5;
        public const int maxColumnForTeam = 3;

        public const int leftTeamId = 1;
        public const int rightTeamId = 2;

        public const int maxUnitHP = 500;

        public const int minDamageUnit = 5;
        public const int maxDamageUnit = 10;

        public const float speedAttackUnit = 0.1f;
        public const float speedMoveUnit = 0.5f;

        public static float GetWidthByTeamId(int teamId) {
            switch (teamId) {
                case leftTeamId: {
                    return 8;
                }
                case rightTeamId: {
                    return 4;
                }
                default:
                    return 0;
            }
        }

        public static Color GetColorByTeamId(int teamId) {
            switch (teamId) {
                case leftTeamId: {
                    return Color.blue;
                }
                case rightTeamId: {
                    return Color.red;
                }
                default:
                    return Color.white;
            }
        }
    }
}