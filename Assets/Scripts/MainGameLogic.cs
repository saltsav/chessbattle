﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts {
    public class MainGameLogic : MonoBehaviour {
        public static MainGameLogic inst;
        public Button startBtn;//МОЖНО ЛУЧШЕ элементы UI необходимо вынести в отдельный класс.

        public void Awake() {
            inst = this;
        }

        public void Start() {
            startBtn.onClick.RemoveAllListeners();
            startBtn.onClick.AddListener(() => {
                BattleController.inst.GenerationTeams();
                SetStartButtonActive(false);
            });

            FieldController.inst.GenerationField();
        }

        public void SetStartButtonActive(bool var) {
            startBtn.gameObject.SetActive(var);
        }

        public void GameEnd() {
            UnitsController.inst.ClearAllUnit();
            FieldController.inst.SetEmptyAllCellOnField();
            SetStartButtonActive(true);
        }
    }
}