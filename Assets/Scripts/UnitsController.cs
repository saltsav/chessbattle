﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts {
    public class UnitsController : MonoBehaviour {
        public static UnitsController inst;
        public RectTransform parentUnits;
        public List<UnitOnField> listAllUnitOnFields;

        public CellOnField testCellOnField;

        public int indexLastUnit = 0;

        public void Awake() {
            inst = this;
        }

        public void SetStateToAllUnits(StateUnit stateUnit) {
            foreach (var unitOnField in listAllUnitOnFields) {
                unitOnField.SetStateUnit(stateUnit);
            }
        }

        public void CreateTeam(int teamId, int countUnits, int minColumn, int maxColumn) {
            for (int i = 0; i < countUnits; i++) {
                var emptyCell = FieldController.inst.GetRandomEmptyCellOnFieldByColumn(Random.Range(minColumn, maxColumn));
                testCellOnField = emptyCell;
                if (emptyCell != null) {
                    emptyCell.isEmpty = false;
                    CreateUnit(emptyCell, teamId);
                }
            }
        }

        public void CreateUnit(CellOnField cellOnField, int teamId) {
            var unitOnField = PrefabControllers.inst.GetUnitOnField();
            listAllUnitOnFields.Add(unitOnField);
            unitOnField.Setup(cellOnField, teamId, indexLastUnit++);
            unitOnField.transform.SetParent(parentUnits);
            unitOnField.transform.localScale = Vector3.one;
            unitOnField.gameObject.SetActive(true);
        }

        public void ClearAllUnit() {
            foreach (var unit in listAllUnitOnFields) {
                if (unit != null) {
                    Destroy(unit.gameObject);
                }
            }
            listAllUnitOnFields.Clear();
        }

        public List<UnitOnField> GetAllEnemyForId(int teamId) {
            var allEnemy = new List<UnitOnField>();
            foreach (var unitOnField in listAllUnitOnFields) {
                if (unitOnField.teamId != teamId && unitOnField.currentStateUnit != StateUnit.die) {
                    allEnemy.Add(unitOnField);
                }
            }

            return allEnemy;
        }

        public void UnitOnFieldDie(UnitOnField unitOnField) {
           var cellOnField = FieldController.inst.GetCellOnFieldByXandY(unitOnField.currentCellOnField.x, unitOnField.currentCellOnField.y);
            cellOnField.isEmpty = true;
            listAllUnitOnFields.Remove(unitOnField);
            unitOnField.StopMoveCoro();
            Destroy(unitOnField.gameObject);
            CheckEndGame();
        }

        public void CheckEndGame() {
            var haveLeftTeam = false;
            var haveRightTeam = false;
            foreach (var unitOnField in listAllUnitOnFields) {
                if (unitOnField.teamId == GP.leftTeamId) {
                    haveLeftTeam = true;
                }
                if (unitOnField.teamId == GP.rightTeamId) {
                    haveRightTeam = true;
                }
            }

            if (!haveLeftTeam || !haveRightTeam) {
                MainGameLogic.inst.GameEnd();
            }
        }
    }
}