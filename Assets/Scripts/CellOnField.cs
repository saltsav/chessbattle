﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts {
    public class CellOnField : MonoBehaviour {
        public RectTransform rectTransform;
        public Image backImage;
        public bool isEmpty;
        [ReadOnly] public int x;
        [ReadOnly] public int y;

        public void Setup(int newX, int newY) {
            x = newX;
            y = newY;
            isEmpty = true;
            name = "cellOnField_x=" + x + "_y=" + y;
            if ((x + y) % 2 != 0) {
                backImage.color = GP.colorGray;
            } else {
                backImage.color = Color.white;
            }
        }

        public Vector3 GetPosition() {
            return rectTransform.position;
        }
    }
}