﻿using System.Collections;
using UnityEngine;

namespace Assets.Scripts {
    public class MainAnimator : MonoBehaviour {
        public static MainAnimator inst;

        public void Awake() {
            inst = this;
        }

        public IEnumerator IEnumMoveTransform(RectTransform rt, Vector2 finalPos, float time, float pow) {
            var startPos = rt.position;
            var currentTime = 0f;
            var t = 0f;
            while (currentTime < time) {
                if (rt == null) {
                    yield break;
                }
                t = currentTime / time;
                rt.position = Vector2.Lerp(startPos, finalPos, Mathf.Pow(t, pow));
                currentTime += Time.unscaledDeltaTime;

                yield return null;
                
            }

            rt.position = finalPos;

            yield return null;
        }
    }
}