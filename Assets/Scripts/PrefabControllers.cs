﻿using UnityEngine;

namespace Assets.Scripts {
    public class PrefabControllers : MonoBehaviour {
        public static PrefabControllers inst; 
        //МОЖНО ЛУЧШЕ префабы назначены через экземпляр на сцене, а значит все в памяти. Необходимо префабы (все ресурсы) хранить в Resources и загружать/выгружать по необходимости
        [SerializeField] private UnitOnField prefabUnitOnField;
        [SerializeField] private CellOnField prefabCellOnField;

        public void Awake() {
            inst = this;
        }

        public UnitOnField GetUnitOnField() {
            return Instantiate(prefabUnitOnField);
        }

        public CellOnField GetCellOnField() {
            return Instantiate(prefabCellOnField);
        }
    }
}