﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts {
    public class FieldController : MonoBehaviour {
        public static FieldController inst;
        public RectTransform parentCells;
        private List<CellOnField> listCellOnFields = new List<CellOnField>();
        public GridLayoutGroup gridLayoutGroup; //МОЖНО ЛУЧШЕ использования GridLayoutGroup применяется для простоты вёрстки поля. Необходимо это делать кодом.

        public void Awake() {
            inst = this;
        }

        public List<CellOnField> GetListCellOnFields() {
            return listCellOnFields;
        }

        public void GenerationField() {
            gridLayoutGroup.constraintCount = GP.fieldWidth;
            for (int y = 0; y < GP.fieldHeight; y++) {
                for (int x = 0; x < GP.fieldWidth; x++) {
                    var cellOnField = PrefabControllers.inst.GetCellOnField();
                    listCellOnFields.Add(cellOnField);
                    cellOnField.Setup(x,y);
                    cellOnField.transform.SetParent(parentCells);
                    cellOnField.transform.localScale = Vector3.one;
                    cellOnField.gameObject.SetActive(true);
                }
            }
        }

        public CellOnField GetRandomEmptyCellOnFieldByColumn(int indexColumn) {
            var listEmptyCellOnField = new List<CellOnField>();

            foreach (var cellOnField in listCellOnFields) {
                if (cellOnField.isEmpty && cellOnField.x == indexColumn) {
                    listEmptyCellOnField.Add(cellOnField);
                }
            }

            if (listEmptyCellOnField.Count == 0) {
                return null;
            }

            return listEmptyCellOnField[Random.Range(0, listEmptyCellOnField.Count)];
        }

        public void SetEmptyAllCellOnField() {
            foreach (var cellOnField in listCellOnFields) {
                cellOnField.isEmpty = true;
            }
        }

        public CellOnField GetCellOnFieldByXandY(int x, int y) {
            foreach (var cellOnField in listCellOnFields) {
                if (cellOnField.x == x && cellOnField.y == y) {
                    return cellOnField;
                }
            }

            return null;
        }
    }
}